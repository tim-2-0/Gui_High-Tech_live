// Serveur (géré par moi) renvoyant l'état du stream
var liveStatusUrl = "http://dotim.livehost.fr/guihightech/live.php";
var stream_name;
var is_notified;
var video_id;

// Fonction pour récupérer l'état du stream
function callUrlJSON(url, callback) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() {
        if (xmlHttp.readyState == 4)
            callback(xmlHttp.responseText);
    }
    xmlHttp.open("GET", url, true);
    xmlHttp.send(null);
}

// Check if ytber is online
function checkLive() {
    console.log("Checking live!");
    // check status from url and parse result to JSON obj
    callUrlJSON(liveStatusUrl, function(result) {
        var jsonObj = JSON.parse(result);
        // Live ?
        if (jsonObj.live) {
            // Stockage nom du live
            stream_name = jsonObj.stream_name;
			// Stockage ID de la vidéo (live)
			video_id = jsonObj.video_id;
            console.log("Stream !");
            live(true);
        } else {
            console.log("Pas de stream !");
            live(false);
        }
    });
}

checkLive();
// Check si live à intervale de 30 secondes
setInterval(checkLive, 30000);

function getNotificationId() {
    var id = Math.floor(Math.random() * 9007199254740992) + 1;
    return id.toString();
}

function live(status) {
    if (status) {
        if (!is_notified) {
			
			// Affichage d'une notification
            chrome.notifications.create(getNotificationId(), {
				type: 'basic',
                title: 'En live !',
                iconUrl: '/img/icon_128.png',
                message: stream_name
            }, function() {});

            chrome.notifications.onClicked.addListener(function() {

                chrome.tabs.create({
                    url: 'https://www.youtube.com/watch?v='+ video_id
                });

            });
			
			// Changement du badge (icone en haut à droite)
			browser.browserAction.setBadgeText({text: 'Live'});

			// Lecture d'un petit son sympathique =D
            var myAudio = new Audio();
            myAudio.src = "sounds/notification.mp3";
            myAudio.play();
        }

        is_notified = true;
    } else {
        is_notified = false;
		// Enlève badge "Live" sur l'icone en haut à droite
		browser.browserAction.setBadgeText({text: ''});
    }
}
