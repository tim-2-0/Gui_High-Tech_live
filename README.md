Extension pour le moment seulement diponnible sous Firefox.

Nom: Gui High-Tech Live ?
Description: Marre de rater le début des lives de Gui High-Tech ? Grâce à cette extension, vous serez notifé lors du lancement d'un live !

Lien: https://addons.mozilla.org/fr/firefox/addon/gui-high-tech-live/

Distribué sous Mozilla Public License, version 2.0